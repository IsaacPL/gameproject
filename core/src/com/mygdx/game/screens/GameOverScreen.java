package com.mygdx.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.GameClass;

public class GameOverScreen implements Screen {
    private GameClass game;
    private Texture texture;
    private float opacity;
    private Color textureColor;

    public GameOverScreen(GameClass game) {
        this.game = game;
        texture = new Texture("GameOver.png");
    }

    public void handleInput() {
        if (Gdx.input.justTouched()) {
            GameClass.intentos++;
            game.setScreen(new PlayScreen(game));
            dispose();
        }
    }

    public void update(float dt) {
        if (opacity < 1)
            opacity += dt;
        handleInput();
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        textureColor = game.sb.getColor();

        game.sb.begin();
        game.sb.setColor(textureColor.r,textureColor.g,textureColor.b,opacity);
        game.sb.draw(texture,0,0);
        game.sb.end();

        update(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
