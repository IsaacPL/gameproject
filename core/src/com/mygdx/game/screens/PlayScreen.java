package com.mygdx.game.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Controller;
import com.mygdx.game.GameClass;
import com.mygdx.game.scenes.Hud;
import com.mygdx.game.sprites.Character;
import com.mygdx.game.sprites.Enemy;
import com.mygdx.game.tools.B2WorldCreator;

import java.util.ArrayList;

public class PlayScreen implements Screen {
    private GameClass game;
    private TextureAtlas textureAtlas;

    private OrthographicCamera camera;
    private Viewport gamePort;
    private Hud hud;

    // Variables del Tiled map
    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //  Variables del Box2d
    private World world;
    private Box2DDebugRenderer b2dr;

    //  Player
    private Character player;

    private Controller controller;

    //aaaaaaaaaaEnemy
    private Enemy enemy1, enemy2;
    private ArrayList<Enemy> enemies;


    private float initialTime;
    private int INITIAL_TIME;

    private TextureRegion darknessOpacity;

    public PlayScreen(GameClass game) {
        textureAtlas = new TextureAtlas("characterAtlas.atlas");
        this.game = game;

        TextureAtlas dummy = new TextureAtlas("goblin.atlas");
        darknessOpacity = new TextureRegion(dummy.findRegion("darkness_opacity"));

        //  Instanciar cámara que sigue al personaje a través de la cámara del mundo
        camera = new OrthographicCamera();

        //  ViewPort que mantiene el ratio de pantalla independientemente del ratio
        //  del dispositivo. Añade barras negras donde no renderiza nada.
        gamePort = new FitViewport(GameClass.V_WIDTH / GameClass.PPM, GameClass.V_HEIGHT / GameClass.PPM, camera);

        //  HUD donde se muestra la puntuación/tiempo/información del nivel
        hud = new Hud(game.sb);

        // Cargar el mapa y configurar el renderizador del mapa
        mapLoader = new TmxMapLoader();
        //map = mapLoader.load("level1.tmx");
        map = mapLoader.load("mapa.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / GameClass.PPM);

        //  Setar inicialmente la cámara centrada correctamente al inicio de
        camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        //world = new World(new Vector2(0, -10), true);
        world = new World(new Vector2(0, 0), true);
        b2dr = new Box2DDebugRenderer();

        new B2WorldCreator(world, map);

        // Setar personaje, controles
        player = new Character(world, this);
        controller = new Controller();


        //aaaaaaaaaa Enemydummy
        enemy1 = new Enemy(world, this, dummy, 200, 300);
        enemy1.body.setLinearVelocity(0.5f, 0);

        enemy2 = new Enemy(world, this, dummy, 200, 100);
        enemy1.body.setLinearVelocity(0.5f, 0);


        //bbbbbbbbbbbbbbbbbb
        player.body.getFixtureList().get(0).setUserData(player);
        enemy1.body.getFixtureList().get(0).setUserData(enemy1);
        enemy2.body.getFixtureList().get(0).setUserData(enemy2);

        enemies = new ArrayList<Enemy>();

        enemies.add(enemy1);
        enemies.add(enemy2);


        initialTime = 20;
        INITIAL_TIME = (int)initialTime;

        System.out.println("Intento nº" + GameClass.intentos);
    }

    public TextureAtlas getAtlas() {
        return textureAtlas;
    }

    @Override
    public void show() {

    }

    public void handleInput(float dt) {
        /*// aaaaaaaaaaaaaaaaaa
        if (!player.isAlive()) game.setScreen(new GameOverScreen(game));
            //*/
        if (!controller.isUpPressed() || !controller.isDownPressed() || !controller.isRightPressed() || !controller.isLeftPressed()) {
            player.body.setLinearVelocity(new Vector2(0, 0));
        }
        if (controller.isUpPressed()) {
            // player.body.applyLinearImpulse(new Vector2(0, 0.5f), player.body.getWorldCenter(), true);
            //player.body.applyLinearImpulse(new Vector2(0, 0.1f), player.body.getWorldCenter(), true);
            player.body.setLinearVelocity(new Vector2(0, 0.7f));
            //controller.
        }
        if (controller.isRightPressed())
            //player.body.applyLinearImpulse(new Vector2(0.1f, 0), player.body.getWorldCenter(), true);
            player.body.setLinearVelocity(new Vector2(0.7f, 0));
        if (controller.isLeftPressed())
            //player.body.applyLinearImpulse(new Vector2(-0.1f, 0), player.body.getWorldCenter(), true);
            player.body.setLinearVelocity(new Vector2(-0.7f, 0));
        if (controller.isDownPressed())
            player.body.setLinearVelocity(new Vector2(0, -0.7f));

        //Provisional
        if (controller.isAttackPressed() && !player.getCurrentlyAttacking()) {
            controller.getStage().getRoot().setTouchable(Touchable.disabled);
            if (player.isWalkingRight()) {
                player.setCurrentSwordXPosition(player.body.getPosition().x - 5, player.body.getPosition().y - 5);
            } else {
                player.setCurrentSwordXPosition(player.body.getPosition().x, player.body.getPosition().y - 5);
            }
            player.triggerAttack(controller);
            //controller.getStage().getRoot().setTouchable(Touchable.disabled);

        }

        //debug muerte
        if (controller.isDeathPressed()) {
            controller.removeInputLayout();
            player.killCharacter();
        }
        //
    }

    public void update(float dt) {
        world.step(1 / 60f, 6, 2);

        player.update(dt);
        enemy1.update(dt);
        enemy2.update(dt);

        camera.position.x = player.body.getPosition().x;
        camera.position.y = player.body.getPosition().y;
        //camera.position.y = player.body.getPosition().y;

        camera.update();
        renderer.setView(camera);

        if (player.isAlive()) {
            hud.updateLabels(player.getCharacterHP(), initialTime, INITIAL_TIME);
            initialTime -= dt;
            if (initialTime <= 0)
                player.killCharacter();
        } else {
            hud.updateLabels(0, initialTime, INITIAL_TIME);
            controller.removeInputLayout();
        }

        if (!player.isAlive() && player.getGameOverLapse()) {
            game.setScreen(new GameOverScreen(game));
            //dispos
        } else {
            handleInput(dt);
            checkCollisions(dt);
        }
    }

    @Override
    public void render(float delta) {
        update(delta);

        //  Pinta la pantalla de negro
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Renderizar el mapa del juego
        renderer.render();

        // Renderizar el Box2DebugLines
        b2dr.render(world, camera.combined);

        game.sb.setProjectionMatrix(camera.combined);
        game.sb.begin();
        //aaaaaaaadummy
        enemy1.draw(game.sb);
        enemy2.draw(game.sb);
        player.draw(game.sb);


        //prueba textura oscuridad
        //game.sb.draw(darknessOpacity,camera.position.x - gamePort.getScreenWidth()/2,camera.position.y - gamePort.getScreenHeight()/2);
        //final prueba

        //aaaaaaadummy
        game.sb.end();

        //Si el dispositivo es Android aparece el layout, si no desaparece
        //if (Gdx.app.getType() == Application.ApplicationType.Android)
        controller.draw();

        //  Setear el SpriteBatch para dibujar lo que ve la cámara
        game.sb.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        b2dr.dispose();
        hud.dispose();
        world.dispose();
        map.dispose();
        renderer.dispose();
    }

    //vidas
    int aux = 3;

    //
    public void checkCollisions(final Float dt) {
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {/*
                if (contact.getFixtureA().getUserData() == player && contact.getFixtureB().getUserData() == enemy1) {
                    if (aux > 0) {
                        aux--;
                        //controller.getStage().getRoot().setTouchable(Touchable.disabled);
                        player.setWounded(true);
                        player.setDefaultCharacterColor(new Color(player.getColor()));
                        //Cambia el color del pj a rojo y lo tira hacia atrás
                        player.body.setLinearVelocity(player.body.getPosition().x*(-250*dt),player.body.getPosition().y*(-250*dt));
                            //player.setColor(255,0,0,1);
                            System.out.println("Colisión, te quedan " + aux + " vidas.");
                        //player.setColor(c);
                    } else {
                        //System.out.println("El personaje ha tocado al enemigo");
                        if (player.body.getFixtureList().size > 0)
                            player.body.getFixtureList().removeRange(0,1);
                        player.killCharacter();
                        controller.removeInputLayout();
                    }
                }
                //provisional colisión espada casi hecho falta la animación de muerte del enemigo
                if (contact.getFixtureA().isSensor() && contact.getFixtureB().getUserData() == enemy1 && player.getCurrentlyAttacking()) {
                    System.out.println("heyyyyyyyyyyyyyyyyyyyyyyy");
                    if (enemy1.body.getFixtureList().size > 0)
                        //System.out.println(enemy1.body.getFixtureList().get(0));
                        //enemy1.body.destroyFixture(enemy1.body.getFixtureList().get(0));
                    //System.out.println(enemy1.body.getFixtureList().get(0));
                    enemy1.killCharacter();
                }*/


                for (Enemy e : enemies) {
                    if (contact.getFixtureA().getUserData() == player && contact.getFixtureB().getUserData() == e) {
                        player.substractCharacterHP();
                        if (player.getCharacterHP() > 0) {
                            //controller.getStage().getRoot().setTouchable(Touchable.disabled);
                            player.setWounded(true);
                            player.setDefaultCharacterColor(new Color(player.getColor()));
                            //Cambia el color del pj a rojo y lo tira hacia atrás
                            player.body.setLinearVelocity(player.body.getPosition().x * (-250 * dt), player.body.getPosition().y * (-250 * dt));
                            //player.setColor(255,0,0,1);
                            System.out.println("Colisión, te quedan " + aux + " vidas.");
                            //player.setColor(c);
                        } else {
                            //System.out.println("El personaje ha tocado al enemigo");
                            if (player.body.getFixtureList().size > 0)
                                player.body.getFixtureList().removeRange(0, 1);
                            player.killCharacter();
                            controller.removeInputLayout();
                        }
                    }
                    //provisional colisión espada casi hecho falta la animación de muerte del enemigo
                    if (contact.getFixtureA().isSensor() && contact.getFixtureB().getUserData() == e && player.getCurrentlyAttacking()) {
                        System.out.println("heyyyyyyyyyyyyyyyyyyyyyyy");
                        if (enemy1.body.getFixtureList().size > 0)
                            //System.out.println(enemy1.body.getFixtureList().get(0));
                            //enemy1.body.destroyFixture(enemy1.body.getFixtureList().get(0));
                            //System.out.println(enemy1.body.getFixtureList().get(0));
                            e.killCharacter();
                    }
                }
            }


            @Override
            public void endContact(Contact contact) {
                //System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa");
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    public Controller getController() {
        return controller;
    }
}
