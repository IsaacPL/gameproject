package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.sprites.Character;
import com.mygdx.game.sprites.Enemy;

public class Controller {
    Viewport viewport;
    Stage stage;
    TextureAtlas atlas;
    TextureRegion region;
    Image upButton, downButton, leftButton, rightButton, botonAtaqueProvisional;
    Character player;
    boolean upPressed, downPressed, leftPressed, rightPressed, attackPressed;
    //debug variable muerte
    boolean deathPressed;
    //

    OrthographicCamera camera;

    public Controller() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(800, 480, camera);
        stage = new Stage(viewport, GameClass.sb);

        stage.addListener(new InputListener() {

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.W:
                        upPressed = true;
                        break;
                    case Input.Keys.S:
                        downPressed = true;
                        break;
                    case Input.Keys.A:
                        leftPressed = true;
                        break;
                    case Input.Keys.D:
                        rightPressed = true;
                        break;
                    case Input.Keys.SPACE:
                        attackPressed = true;
                        break;
                    //debug muerte
                    case Input.Keys.K:
                        deathPressed = true;
                        break;
                    //
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.W:
                        upPressed = false;
                        break;
                    case Input.Keys.S:
                        downPressed = false;
                        break;
                    case Input.Keys.A:
                        leftPressed = false;
                        break;
                    case Input.Keys.D:
                        rightPressed = false;
                        break;
                    case Input.Keys.SPACE:
                        attackPressed = false;
                        break;
                    //debug muerte
                    case Input.Keys.K:
                        deathPressed = false;
                        break;
                    //
                }
                return true;
            }
        });

        Gdx.input.setInputProcessor(stage);

        Table table = new Table();
        table.left().bottom();

        atlas = new TextureAtlas("texturesAtlas.atlas");
        region = atlas.findRegion("upoff");

        upButton = new Image(region);
        upButton.setSize(70, 80);
        upButton.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                upPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                upPressed = false;
            }
        });

        region = atlas.findRegion("downoff");

        downButton = new Image(region);
        downButton.setSize(70, 80);
        downButton.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                downPressed = true;
                //downButton = new Image(new TextureRegion(atlas.findRegion("downon")));
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                downPressed = false;
            }
        });

        region = atlas.findRegion("rightoff");

        rightButton = new Image(region);
        rightButton.setSize(70, 80);
        rightButton.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = true;
                //rightButton.remove();

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = false;
            }
        });

        region = atlas.findRegion("leftoff");

        leftButton = new Image(region);
        leftButton.setSize(70, 80);
        leftButton.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = false;
            }
        });

        region = atlas.findRegion("righton");

        botonAtaqueProvisional = new Image(region);
        botonAtaqueProvisional.setSize(70, 80);
        botonAtaqueProvisional.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                attackPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                attackPressed = false;
            }
        });

        table.add();
        table.add(upButton).size(upButton.getWidth(), upButton.getHeight());
        table.add();
        table.row().pad(5, 5, 5, 5);
        table.add(leftButton).size(leftButton.getWidth(), leftButton.getHeight());
        table.add();
        table.add(rightButton).size(rightButton.getWidth(), rightButton.getHeight());
        //provisional
        table.add();
        table.add(botonAtaqueProvisional).size(botonAtaqueProvisional.getWidth(), botonAtaqueProvisional.getHeight()).padLeft(300);
        //
        table.row().padBottom(5);
        table.add();
        table.add(downButton).size(downButton.getWidth(), downButton.getHeight());
        table.add();
        table.padLeft(50);

        table.pack();

        stage.addActor(table);
    }

    public void draw() {
        stage.draw();
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isDownPressed() {
        return downPressed;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    public boolean isAttackPressed() {
        return attackPressed;
    }

    //debug muerte
    public boolean isDeathPressed() {
        return deathPressed;
    }
    //

    public void removeInputLayout() {
        stage.clear();
    }

    public Stage getStage() {
        return stage;
    }
}
