package com.mygdx.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Controller;
import com.mygdx.game.GameClass;
import com.mygdx.game.screens.PlayScreen;

public class Enemy extends Sprite {
    private Controller controller;

    public enum State {WALKING, STANDING, ATTACKING, DEAD}

    public Character.State currentState, previousState;
    public World world;
    public Body body;
    private TextureRegion characterStandIdleInitialFrame;
    private Animation<TextureRegion> characterWalk, characterStandIdle, characterAttack, characterDeath;
    private float stateTimer;
    private boolean walkingRight, isAttacking, isAlive;

    //aaaaa
    private Sound footStep1, footStep2, attack, chainmail1;
    private boolean playingSoundMovement, playingSoundAttacking;
    private int frameFootStepsSound;

    private float xPosition, yPosition;
    //

    public Enemy(World world, PlayScreen screen, TextureAtlas dummy, float xPosition, float yPosition) {
        //super(screen.getAtlas().findRegion("goblin"));
        super(dummy.findRegion("goblin_spritesheet_calciumtrice_grande"));
        this.world = world;

        this.xPosition = xPosition;
        this.yPosition = yPosition;

        currentState = Character.State.STANDING;
        previousState = Character.State.STANDING;
        stateTimer = 0;
        walkingRight = true;
        isAttacking = false;
        isAlive = true;

        //aa
        footStep1 = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/footstep01.mp3"));
        chainmail1 = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/chainmail1.mp3"));
        playingSoundMovement = false;
        attack = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/sword2.mp3"));
        playingSoundMovement = false;
        frameFootStepsSound = 0;

        //

        //  Animación idle
        Array<TextureRegion> frames = new Array<TextureRegion>();
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 0, 160, 162));
        characterStandIdle = new Animation(0.15f, frames);
        frames.clear();

        //  Animación caminar
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 1340, 160, 160));
        characterWalk = new Animation(0.125f, frames);
        frames.clear();

        // Animación atacar
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 161 * 3, 160, 160));
        characterAttack = new Animation(0.09f, frames);
        frames.clear();

        // Animación morir
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 1660, 170, 160));
        characterDeath = new Animation(0.15f, frames);
        frames.clear();

        //  Frame inicial
        characterStandIdleInitialFrame = new TextureRegion(getTexture(), 0, 0, 160, 160);

        defineCharacter();
        setBounds(0, 0, characterStandIdleInitialFrame.getRegionWidth() / 2 / GameClass.PPM, characterStandIdleInitialFrame.getRegionHeight() / 2 / GameClass.PPM);
        setRegion(characterStandIdleInitialFrame);

        //aaaaaaaaaaaaaaaaaaaaaaaaaaa
        //body.getPosition().x
        //
    }

    public void update(float dt) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
        setRegion(getFrame(dt));

        //body.setLinearVelocity(new Vector2(0.5f,0));
        movementAnimation();

/*        if (currentState == Character.State.ATTACKING) {
            // Casi parar animación al acabar
            if (characterAttack.getKeyFrameIndex(stateTimer) == 4 && !playingSoundAttacking) {
                playingSoundAttacking = true;
                attack.setVolume(attack.play(),0.3f);
            } else if (characterAttack.getKeyFrameIndex(stateTimer) == 6 && playingSoundMovement) {
                playingSoundAttacking = false;
            }

            if (characterAttack.isAnimationFinished(stateTimer)) {
                endAttack();
                controller.getStage().getRoot().setTouchable(Touchable.enabled);
                playingSoundAttacking = false;
            }
            //
        } else if (currentState == Character.State.WALKING) {
            if (characterWalk.getKeyFrameIndex(stateTimer) == 4 && !playingSoundMovement) {
                playingSoundMovement = true;
                //footStep1.play();
                footStep1.setVolume(footStep1.play(),0.1f);
                chainmail1.setVolume(chainmail1.play(),0.03f);
            } else if (characterWalk.getKeyFrameIndex(stateTimer) == 5 && playingSoundMovement) {
                playingSoundMovement = false;
            }
        }*/
    }

    public TextureRegion getFrame(Float dt) {
        currentState = getState();

        TextureRegion region;
        switch (currentState) {
            case WALKING:
                region = characterWalk.getKeyFrame(stateTimer, true);
                //System.out.println(characterWalk.getKeyFrameIndex(stateTimer) + " " + stateTimer);
                break;
            case ATTACKING:
                region = characterAttack.getKeyFrame(stateTimer, false);
                // Casi parar animación al acabar
                //
                break;
            case DEAD:
                region = characterDeath.getKeyFrame(stateTimer, false);
                // Casi parar animación al acabar
                //if (characterDeath.isAnimationFinished(stateTimer)) killCharacter();
                //
                if (stateTimer >= 0.1 && this.body.getFixtureList().size > 0)
                    this.body.destroyFixture(this.body.getFixtureList().get(0));
                break;
            case STANDING:
            default:
                region = characterStandIdle.getKeyFrame(stateTimer, true);
        }

        //Actualizar la dirección a la que apuntó por última vez el personaje
        if ((body.getLinearVelocity().x < 0 || !walkingRight) && !region.isFlipX()) {
            region.flip(true, false);
            walkingRight = false;
        } else if ((body.getLinearVelocity().x > 0 || walkingRight) && region.isFlipX()) {
            region.flip(true, false);
            walkingRight = true;
        }
/*
        if (currentState == State.ATTACKING && stateTimer == characterAttack.getAnimationDuration())
            triggerAttack();*/
        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        previousState = currentState;

        return region;
    }

    private Character.State getState() {
        //prueba debug muerte
        if (!isAlive) {
            this.body.setType(BodyDef.BodyType.StaticBody);
            return Character.State.DEAD;
            //
        } else {
            if (body.getLinearVelocity().x != 0 || body.getLinearVelocity().y != 0)
                return Character.State.WALKING;
            //provisional
            if (isAttacking)
                return Character.State.ATTACKING;
            else
                return Character.State.STANDING;
        }
    }

    public void defineCharacter() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(xPosition / GameClass.PPM, yPosition / GameClass.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
/*        CircleShape circleShape = new CircleShape();
        //el objeto será más grande contra mayor sea n / GameClass.PPM
        circleShape.setRadius(10 / GameClass.PPM);

        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef);*/


        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(12 / GameClass.PPM, 16 / GameClass.PPM);
        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);
    }

    public boolean getCurrentlyAttacking() {
        return isAttacking;
    }

    public void triggerAttack(Controller controller) {
        isAttacking = true;
        this.controller = controller;
    }

    public void endAttack() {
        isAttacking = false;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void killCharacter() {
        isAlive = false;

        //this.body.destroyFixture(this.body.getFixtureList().get(0));
    }

    public void movementAnimation() {
        if (body.getLinearVelocity().x == 0 && walkingRight) {
            body.setLinearVelocity(-0.5f, 0);
        }

        if (body.getLinearVelocity().x == 0 && !walkingRight) {
            body.setLinearVelocity(0.5f, 0);
        }
    }
}
