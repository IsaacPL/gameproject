package com.mygdx.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Controller;
import com.mygdx.game.GameClass;
import com.mygdx.game.screens.GameOverScreen;
import com.mygdx.game.screens.PlayScreen;

public class Character extends Sprite {
    int characterHP;

    public void substractCharacterHP() {
        characterHP--;
    }

    public int getCharacterHP() {
        return characterHP;
    }

    private Controller controller;

    public boolean getGameOverLapse() {
        return gameOverSeconds >= 4;
    }

    public enum State {WALKING, STANDING, ATTACKING, DEAD}

    public State currentState, previousState;
    public World world;
    public Body body;
    private TextureRegion characterStandIdleInitialFrame;
    private Animation<TextureRegion> characterWalk, characterStandIdle, characterAttack, characterDeath;
    private float stateTimer;

    public boolean isWalkingRight() {
        return walkingRight;
    }

    private boolean walkingRight, isAttacking, isAlive;

    //aaaaa
    private Sound footStep1, footStep2, attack, chainmail1;
    private boolean playingSoundMovement1, playingSoundMovement2, playingSoundAttacking;
    private float frameFootStepsSound1, frameFootStepsSound2;
    private TextureRegion footStepSoundKeyFrame1, footStepSoundKeyFrame2;

    private PolygonShape sword;
    private boolean swordFlagCollider;
    private float swordFrameFlag;

    private Color defaultCharacterColor;

    float gameOverSeconds;

    public void setDefaultCharacterColor(Color defaultCharacterColor) {
        this.defaultCharacterColor = defaultCharacterColor;
    }

    public void setWounded(boolean wounded) {
        this.wounded = wounded;
    }

    private boolean wounded;
    private float woundedLapse;

    public float getCurrentSwordXPosition() {
        return currentSwordXPosition;
    }

    public void setCurrentSwordXPosition(float currentSwordXPosition, float currentSwordYPosition) {
        this.currentSwordXPosition = currentSwordXPosition;
        this.currentSwordYPosition = currentSwordYPosition;
    }

    private float currentSwordXPosition;
    private float currentSwordYPosition;
    //

    public Character(World world, PlayScreen screen) {
        super(screen.getAtlas().findRegion("character_spritesheet_grande"));
        this.world = world;
        currentState = State.STANDING;
        previousState = State.STANDING;
        stateTimer = 0;
        walkingRight = true;
        isAttacking = false;
        isAlive = true;

        //aa
        footStep1 = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/footstep01.ogg"));
        footStep2 = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/footstep02.ogg"));
        chainmail1 = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/chainmail1.mp3"));
        playingSoundMovement1 = false;
        playingSoundMovement2 = false;
        attack = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/sword2.mp3"));
        playingSoundMovement1 = false;
        frameFootStepsSound1 = 0;
        frameFootStepsSound2 = 0;
        characterHP = 3;
        //

        //  Animación idle
        Array<TextureRegion> frames = new Array<TextureRegion>();
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 0, 160, 162));
        characterStandIdle = new Animation(0.2f, frames);
        frames.clear();

        //  Animación caminar
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 161 * 2, 160, 160));
        characterWalk = new Animation(0.125f, frames);

        //aaa
        footStepSoundKeyFrame1 = frames.get(4);
        footStepSoundKeyFrame2 = frames.get(9);
        //

        frames.clear();

        // Animación atacar
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 161 * 3, 160, 159));
        characterAttack = new Animation(0.09f, frames);
        frames.clear();

        // Animación morir
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 161 * 4 - 2, 170, 160));
        characterDeath = new Animation(0.15f, frames);
        frames.clear();

        //  Frame inicial
        characterStandIdleInitialFrame = new TextureRegion(getTexture(), 0, 0, 160, 160);

        defineCharacter();
        setBounds(0, 0, characterStandIdleInitialFrame.getRegionWidth() / 2 / GameClass.PPM, characterStandIdleInitialFrame.getRegionHeight() / 2 / GameClass.PPM);
        setRegion(characterStandIdleInitialFrame);
    }

    public void update(float dt) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
        setRegion(getFrame(dt));
        statesManager();
        //temporal
        woundLapseManager(dt);
        //
    }


    public TextureRegion getFrame(Float dt) {
        currentState = getState();

        TextureRegion region;
        switch (currentState) {
            case WALKING:
                getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM);
                region = characterWalk.getKeyFrame(stateTimer, true);
                if (!characterWalk.isAnimationFinished(frameFootStepsSound1)) {
                    frameFootStepsSound1 += dt;
                } else {
                    frameFootStepsSound1 = 0;
                }
                if (!characterWalk.isAnimationFinished(frameFootStepsSound2)) {
                    frameFootStepsSound2 += dt;
                } else {
                    frameFootStepsSound2 = 0;
                }
                //System.out.println(characterWalk.getKeyFrameIndex(stateTimer) + " " + characterWalk.getKeyFrame(stateTimer) + " " + stateTimer);
                break;
            case ATTACKING:
                region = characterAttack.getKeyFrame(stateTimer, false);
                // getSword().setAsBox(35 / GameClass.PPM, 10 / GameClass.PPM);
                // Casi parar animación al acabar
                //
                if (characterAttack.getKeyFrameIndex(swordFrameFlag) == 3 && !swordFlagCollider) {
                    swordFlagCollider = true;
                }

                if (characterAttack.getKeyFrameIndex(swordFrameFlag) == 6 && swordFlagCollider) {
                    swordFlagCollider = false;
                }

                swordFrameFlag += dt;
                break;
            case DEAD:
                region = characterDeath.getKeyFrame(stateTimer, false);
                // Casi parar animación al acabar
                //if (characterDeath.isAnimationFinished(stateTimer)) killCharacter();
                //
                gameOverSeconds += dt;
                break;
            case STANDING:
            default:
                region = characterStandIdle.getKeyFrame(stateTimer, true);
                frameFootStepsSound1 = 0;
                frameFootStepsSound2 = 0;
        }

        //Actualizar la dirección a la que apuntó por última vez el personaje
        if ((body.getLinearVelocity().x < 0 || !walkingRight) && !region.isFlipX()) {
            region.flip(true, false);
            walkingRight = false;
        } else if ((body.getLinearVelocity().x > 0 || walkingRight) && region.isFlipX()) {
            region.flip(true, false);
            walkingRight = true;
        }
/*
        if (currentState == State.ATTACKING && stateTimer == characterAttack.getAnimationDuration())
            triggerAttack();*/
        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        previousState = currentState;
        return region;
    }

    private State getState() {
        //prueba debug muerte
        if (!isAlive) {
            this.body.setType(BodyDef.BodyType.StaticBody);
            return State.DEAD;
            //
        } else {
            if (body.getLinearVelocity().x != 0 || body.getLinearVelocity().y != 0)
                return State.WALKING;
            //provisional
            if (isAttacking)
                return State.ATTACKING;
            else
                return State.STANDING;
        }
    }

    public void defineCharacter() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(200 / GameClass.PPM, 200 / GameClass.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
/*        //el objeto será más grande contra mayor sea n / GameClass.PPM
        circleShape.setRadius(10 / GameClass.PPM);

        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef);*/


        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(18 / GameClass.PPM, 25 / GameClass.PPM);
        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);

        FixtureDef fixtureDef2 = new FixtureDef();
        PolygonShape swordShape = new PolygonShape();

        fixtureDef2.isSensor = true;

        swordShape.setAsBox(18 / GameClass.PPM, 10 / GameClass.PPM, new Vector2(body.getPosition().x / GameClass.PPM, body.getPosition().y / GameClass.PPM), 0);
        fixtureDef2.shape = swordShape;
        body.createFixture(fixtureDef2).setSensor(true);
    }

    public boolean getCurrentlyAttacking() {
        return isAttacking;
    }

    public void triggerAttack(Controller controller) {
        isAttacking = true;
        this.controller = controller;
    }

    public void endAttack() {
        isAttacking = false;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void killCharacter() {
        isAlive = false;
    }

    public void statesManager() {
        if (currentState == State.ATTACKING) {
            //getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM,new Vector2((body.getPosition().x+=2000*Gdx.graphics.getDeltaTime())/GameClass.PPM,body.getPosition().y/GameClass.PPM),0);
            // Casi parar animación al acabar
            if (characterAttack.getKeyFrameIndex(stateTimer) == 4 && !playingSoundAttacking) {
                playingSoundAttacking = true;

                attack.setVolume(attack.play(), 0.3f);
            }

            if (isWalkingRight()) {

                if (swordFlagCollider && swordFrameFlag < 6) {
                    getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM, new Vector2((currentSwordXPosition += swordFrameFlag * 4) / GameClass.PPM, currentSwordYPosition / GameClass.PPM), 0);
                }

                if (!swordFlagCollider && characterAttack.getKeyFrameIndex(swordFrameFlag) > 6) {
                    getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM, new Vector2((currentSwordXPosition -= swordFrameFlag) / GameClass.PPM, currentSwordYPosition / GameClass.PPM), 0);
                }
            } else {

                if (swordFlagCollider && swordFrameFlag < 6) {
                    getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM, new Vector2((currentSwordXPosition -= swordFrameFlag * 4) / GameClass.PPM, currentSwordYPosition / GameClass.PPM), 0);
                }

                if (!swordFlagCollider && characterAttack.getKeyFrameIndex(swordFrameFlag) > 6) {
                    getSword().setAsBox(20 / GameClass.PPM, 10 / GameClass.PPM, new Vector2((currentSwordXPosition += swordFrameFlag) / GameClass.PPM, currentSwordYPosition / GameClass.PPM), 0);
                }
            }

            if (characterAttack.isAnimationFinished(stateTimer)) {
                endAttack();/*
                currentSwordXPosition = body.getPosition().x;*/
                swordFrameFlag = 0;
                playingSoundAttacking = false;
                controller.getStage().getRoot().setTouchable(Touchable.enabled);
            }

            //moveSwordCollider(Gdx.graphics.getDeltaTime());
            //
        } else if (currentState == State.WALKING) {
            if (characterWalk.getKeyFrame(frameFootStepsSound1) == footStepSoundKeyFrame1 && !playingSoundMovement1) {
                playingSoundMovement1 = true;
                footStep1.setVolume(footStep1.play(), 0.1f);
                chainmail1.setVolume(chainmail1.play(), 0.03f);
            }

            if (characterWalk.getKeyFrame(frameFootStepsSound2) == footStepSoundKeyFrame2 && !playingSoundMovement2) {
                playingSoundMovement2 = true;
                footStep2.setVolume(footStep2.play(), 0.1f);
                chainmail1.setVolume(chainmail1.play(), 0.03f);
            }


            if ((characterWalk.getKeyFrame(frameFootStepsSound1) != footStepSoundKeyFrame1 && playingSoundMovement1)) {
                playingSoundMovement1 = false;
            }


            if ((characterWalk.getKeyFrame(frameFootStepsSound2) != footStepSoundKeyFrame2 && playingSoundMovement2)) {
                playingSoundMovement2 = false;
            }
        }
    }

    public PolygonShape getSword() {
        return (PolygonShape) body.getFixtureList().get(1).getShape();
    }

    public void woundLapseManager(Float dt) {
        if (wounded && woundedLapse < 0.5) {
            woundedLapse += dt;
            this.setColor(255, 0, 0, 1 * woundedLapse + dt);
        } else {
            if (defaultCharacterColor != null) {
                wounded = false;
                woundedLapse = 0;
                this.setColor(defaultCharacterColor);
            }
        }
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
