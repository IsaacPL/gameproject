package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.screens.MainMenuScreen;
import com.mygdx.game.screens.PlayScreen;

public class GameClass extends Game {
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 200;

	public static int intentos;

	//	Medidas para debuggar poniendo el mapa a mayor resolución
/*		public static final int V_WIDTH = 1080;
		public static final int V_HEIGHT = 1920;*/

	public static final float PPM = 100;
	//public SpriteBatch sb;
	// prueba
	public static SpriteBatch sb;
	@Override
	public void create () {
		sb = new SpriteBatch();
		//setScreen(new PlayScreen(this));
		setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
	}
}
