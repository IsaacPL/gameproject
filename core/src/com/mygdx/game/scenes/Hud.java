package com.mygdx.game.scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.GameClass;

public class Hud implements Disposable {
    public Stage stage;
    private Viewport viewport;

    private Integer worldTImer, score;
    private float timeCount;

    Label countdownLabel, countdownHP, timeLabel, levelLabel, worldLabel, hpLabel;

    public Hud(SpriteBatch sb) {
        worldTImer = 300;
        timeCount = 0;
        score = 0;

        viewport = new FitViewport(GameClass.V_WIDTH*1.25f, GameClass.V_HEIGHT*1.25f, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();
        table.top();
        table.setFillParent(true); //Tamaño del stage

/*        countdownLabel = new Label(String.format("%03d", worldTImer),
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        scoreLabel = new Label(String.format("%06d", worldTImer),
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));*/

        countdownLabel = new Label("UNDEFINED Time",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        countdownHP = new Label("UNDEFINED Hp",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        levelLabel = new Label("UNDEFINED Inner",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        worldLabel = new Label("PLACE",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        hpLabel = new Label("HP",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.add(hpLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(countdownHP).expandX();
        table.add(levelLabel).expandX();
        table.add(countdownLabel).expandX();

        stage.addActor(table);
    }

    public void updateLabels(int currentPlayerHP,float timer,int initialTime) {
        countdownHP.setText(currentPlayerHP);
        countdownLabel.setText((int)timer);

        if (timer <= initialTime*0.4)
            countdownLabel.setColor(Color.RED);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
